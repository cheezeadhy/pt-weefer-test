﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars.Navigation;
using DevExpress.XtraLayout;
using DevExpress.XtraEditors;

namespace nmCtrl
{
    public class clsControl
    {
        public static string GENCodeString3(string tLastData, string tPrefix)
        {
            string tmpNumber;
            string Code = string.Empty;
            int tmpNumberINC;
            string tmpNumberString;

            tmpNumber = tLastData.Substring(tLastData.Length - 3);
            tmpNumberINC = Convert.ToInt32(tmpNumber) + 1;
            tmpNumberString = Convert.ToString(tmpNumberINC).Trim();

            Code = CodeFormat3(tPrefix, tmpNumberString);
            return Code;
        }

        public static string CodeFormat3(string tPrefix, string tString)
        {
            string Code = string.Empty;
            if (tString.Length == 1)
                Code = tPrefix + "00" + tString;
            else if (tString.Length == 2)
                Code = tPrefix + "0" + tString;
            else if (tString.Length == 3)
                Code = tPrefix + tString;

            return Code;
        }

        public static string GENCodeString4(string tLastData, string tPrefix)
        {
            string tmpNumber;
            string Code = string.Empty;
            int tmpNumberINC;
            string tmpNumberString;

            tmpNumber = tLastData.Substring(tLastData.Length - 4);
            tmpNumberINC = Convert.ToInt32(tmpNumber) + 1;
            tmpNumberString = Convert.ToString(tmpNumberINC).Trim();

            Code = CodeFormat4(tPrefix, tmpNumberString);
            return Code;
        }

        public static string CodeFormat4(string tPrefix, string tString)
        {
            string Code = string.Empty;
            if (tString.Length == 1)
                Code = tPrefix + "000" + tString;
            else if (tString.Length == 2)
                Code = tPrefix + "00" + tString;
            else if (tString.Length == 3)
                Code = tPrefix + "0" + tString;
            else if (tString.Length == 4)
                Code = tPrefix + tString;

            return Code;
        }

        public static string GENCodeString6(string tLastData, string tPrefix)
        {
            string tmpNumber;
            string Code = string.Empty;
            int tmpNumberINC;
            string tmpNumberString;

            tmpNumber = tLastData.Substring(tLastData.Length - 6);
            tmpNumberINC = Convert.ToInt32(tmpNumber) + 1;
            tmpNumberString = Convert.ToString(tmpNumberINC).Trim();

            Code = CodeFormat6(tPrefix, tmpNumberString);
            return Code;
        }

        public static string CodeFormat6(string tPrefix, string tString)
        {
            string Code = string.Empty;
            if (tString.Length == 1)
                Code = tPrefix + "00000" + tString;
            else if (tString.Length == 2)
                Code = tPrefix + "0000" + tString;
            else if (tString.Length == 3)
                Code = tPrefix + "000" + tString;
            else if (tString.Length == 4)
                Code = tPrefix + "00" + tString;
            else if (tString.Length == 5)
                Code = tPrefix + "0" + tString;
            else if (tString.Length == 6)
                Code = tPrefix + tString;

            return Code;
        }

        public static string SET_EMPTY_IF_NOTHING(TextEdit txt)
        {
            var tValue = Convert.ToString(txt.EditValue);
            if (tValue == null)
                tValue = string.Empty;
            return tValue;
        }
    }
}
