﻿using DevExpress.Utils;
using DevExpress.Utils.Paint;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nmCtrl
{
    class clsGrid
    {
        public static void dgCommonFormat(GridView dgView)
        {
            var p = dgView;
            {
                p.Columns["No"].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;

                p.Columns["Created_By"].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
                p.Columns["Created_Time"].DisplayFormat.FormatType = FormatType.DateTime;
                p.Columns["Created_Time"].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                p.Columns["Created_Time"].DisplayFormat.FormatString = "dd/MM/yyyy | HH:mm";

                p.Columns["Modified_By"].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
                p.Columns["Modified_Time"].DisplayFormat.FormatType = FormatType.DateTime;
                p.Columns["Modified_Time"].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                p.Columns["Modified_Time"].DisplayFormat.FormatString = "dd/MM/yyyy | HH:mm";

                p.Columns["Created_By"].Visible = false;
                p.Columns["Created_Time"].Visible = false;
                p.Columns["Modified_By"].Visible = false;
                p.Columns["Modified_Time"].Visible = false;
            }
        }

        public static void Custom_Draw_Cell_Number(string tStr, RowCellCustomDrawEventArgs e)
        {
            if (e.Column.Name == tStr && e.RowHandle >= 0)
            {
                e.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        public static void Highlight_Filter(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView dgView = (GridView)(sender);
            string tFilterText = dgView.GetRowCellDisplayText(GridControl.AutoFilterRowHandle, e.Column);
            if (string.IsNullOrWhiteSpace(tFilterText))
            {
                return;
            }

            int tFIlterIndex = e.DisplayText.IndexOf(tFilterText, StringComparison.CurrentCultureIgnoreCase);
            if (tFIlterIndex == -1)
            {
                return;
            }

            XPaint.Graphics.DrawMultiColorString(e.Cache, e.Bounds, e.DisplayText, tFilterText, e.Appearance, Color.Black, Color.Gold, false, tFIlterIndex);
            e.Handled = true;
        }

        public static object Get_Grid_View_Cell_Value(GridView dgView, int tRowIDX, string tColName)
        {
            if (dgView.RowCount > 0)
            {
                //AMBIL NILAI PADA DATAGRID
                var tmpDGValue = dgView.GetRowCellValue(tRowIDX, tColName);
                return tmpDGValue;
            }
            else
                return null;
        }
    }
}
