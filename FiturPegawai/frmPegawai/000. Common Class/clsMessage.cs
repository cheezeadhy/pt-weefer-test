﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace nmCtrl
{
    class clsMessage
    {
        public static void ErrorMessage(Exception ex)
        {
            XtraMessageBox.Show(ex.Message, "Runtime Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void WarningMessage(string tJudul, string tMessage)
        {
            XtraMessageBox.Show(tMessage, "Warning" + tJudul, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void SuccessMessage()
        {
            XtraMessageBox.Show("Data Berhasil Disimpan", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void CustomSuccessMessage(string tMessage)
        {
            XtraMessageBox.Show(tMessage, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static bool Confirmation(string tStr)
        {
            var result = XtraMessageBox.Show(tStr, "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result == DialogResult.OK)
                return true;
            else
                return false;
        }

        public static bool WarningConfirmation(string tStr)
        {
            var result = XtraMessageBox.Show(tStr, "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.OK)
                return true;
            else
                return false;
        }
    }
}
