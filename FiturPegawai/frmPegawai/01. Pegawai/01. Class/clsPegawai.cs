﻿using System;
using System.Linq;
using nmEF;
using System.Data;
using DevExpress.Utils;
using DevExpress.XtraGrid;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using nmCtrl;

namespace nmPegawai
{
    class clsPegawai
    {
        public string tIDPegawai { get; set; }
        public string tNama { get; set; }
        public string tAlamat { get; set; }
        public DateTime tTglLahir { get; set; }
        public string tPendidikanTerakhir { get; set; }
        public bool tStatusAktif { get; set; }

        public static string GEN_CODE()
        {
            using (var context = new EFEntities())
            {
                var query = (from c in context.PEGAWAI orderby c.IDPegawai descending select c.IDPegawai).FirstOrDefault();
                if (query != null)
                {
                    return clsControl.GENCodeString6(query, "PGW");
                }
                else
                {
                    return "PGW000001";
                }
            }
        }

        private static DataTable DT_PEGAWAI()
        {
            var dt = new DataTable();
            dt.Columns.Add("No", typeof(int));
            dt.Columns.Add("IDPegawai", typeof(string));
            dt.Columns.Add("Nama", typeof(string));
            dt.Columns.Add("Alamat", typeof(string));
            dt.Columns.Add("TglLahir", typeof(DateTime));
            dt.Columns.Add("PendidikanTerakhir", typeof(string));
            dt.Columns.Add("StatusPegawai", typeof(bool));
            dt.Columns.Add("Created_By", typeof(string));
            dt.Columns.Add("Created_Time", typeof(DateTime));
            dt.Columns.Add("Modified_By", typeof(string));
            dt.Columns.Add("Modified_Time", typeof(DateTime));
            return dt;
        }

        public static DataTable RETRIEVE_DATA(GridControl dg, GridView dgView )
        {
            using (var context = new EFEntities())
            {
                var dt = new DataTable();
                dt = DT_PEGAWAI();
                dg.DataSource = dt;

                var query = (from c in context.PEGAWAI orderby c.IDPegawai descending where c.StatusPegawai == true select new { No = 1, c.IDPegawai, c.Nama, c.Alamat,c.TglLahir,c.PendidikanTerakhir, c.StatusPegawai, Created_By = c.UserC, Created_Time = c.TimeC, Modified_By = c.UserM, Modified_Time = c.TimeM, Deleted_By = c.UserD, Deleted_Time = c.TimeD });

                if (query.FirstOrDefault() != null)
                {
                    foreach (var r in query.ToList())
                    {
                        dt.Rows.Add(r.No, r.IDPegawai, r.Nama, r.Alamat,r.TglLahir, r.PendidikanTerakhir, r.StatusPegawai, r.Created_By, r.Created_Time, r.Modified_By, r.Modified_Time);
                    }

                    dg.DataSource = dt;
                }
                clsGrid.dgCommonFormat(dgView);
                DG_DFORMAT(dgView);
                dgView.BestFitColumns();

                return dt;
            }
        }

        public static void DG_DFORMAT(GridView dgView)
        {
            var p = dgView;
            {
                p.Columns["PendidikanTerakhir"].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;

                p.Columns["TglLahir"].DisplayFormat.FormatType = FormatType.DateTime;
                p.Columns["TglLahir"].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                p.Columns["TglLahir"].DisplayFormat.FormatString = "dd/MM/yyyy";
            }
        }

        public void CREATE(EFEntities context)
        {
            var ent = new PEGAWAI();
            ent.IDPegawai = tIDPegawai;
            ent.Nama = tNama;
            ent.Alamat = tAlamat;
            ent.TglLahir = tTglLahir;
            ent.PendidikanTerakhir = tPendidikanTerakhir;
            ent.StatusPegawai = tStatusAktif;
            ent.UserC = "Admin";
            ent.TimeC = DateTime.Now;
            context.PEGAWAI.Add(ent);
        }

        public void UPDATE(string tmpIDPegawai, EFEntities context)
        {
            var ent = (from c in context.PEGAWAI where c.IDPegawai == tmpIDPegawai select c).FirstOrDefault();
            if (ent != null)
            {
                ent.Nama = tNama;
                ent.Alamat = tAlamat;
                ent.TglLahir = tTglLahir;
                ent.PendidikanTerakhir = tPendidikanTerakhir;
                ent.StatusPegawai = tStatusAktif;
                ent.UserC = "Admin";
                ent.TimeC = DateTime.Now;
            }
        }

        public static void RETRIEVE_TO_CONTROL(string tmpIDPegawai, TextEdit txtIDPegawai, TextEdit txtNama, TextEdit txtAlamat,DateEdit dtTglLahir, ComboBoxEdit cmbPendidikanTerakhir, CheckEdit chkStatusPegawai)
        {
            using (var context = new EFEntities())
            {
                var ent = (from c in context.PEGAWAI where c.IDPegawai == tmpIDPegawai select c).FirstOrDefault();
                if (ent != null)
                {
                    txtIDPegawai.EditValue = ent.IDPegawai;
                    txtNama.EditValue = ent.Nama;
                    txtAlamat.EditValue = ent.Alamat;
                    dtTglLahir.EditValue = ent.TglLahir;
                    cmbPendidikanTerakhir.EditValue = ent.PendidikanTerakhir;
                    chkStatusPegawai.EditValue = ent.StatusPegawai;

                }
            }
        }

        public static void NON_ACTIVE(string tmpIDPegawai, EFEntities context)
        {
            var ent = (from c in context.PEGAWAI where c.IDPegawai == tmpIDPegawai select c).FirstOrDefault();
            if (ent != null)
            {
                ent.StatusPegawai = false;
                ent.UserM = "Admin";
                ent.TimeM = DateTime.Now;
            }
        }

        public static bool SOFT_DELETE(string tmpIDPegawai)
        {
            using (var context = new EFEntities())
            {
                var ent = (from c in context.PEGAWAI where c.IDPegawai == tmpIDPegawai select c).FirstOrDefault();
                if (ent != null)
                {
                    ent.UserD = "Admin";
                    ent.TimeD = DateTime.Now;
                    context.SaveChanges();
                    return true;
                }

                else
                {
                    clsMessage.WarningMessage("-Delete", "Data digunakan dalam tabel lain, tidak bisa dihapus");
                    return false;
                }
            }
        }

        public static bool VALIDATION(TextEdit txtIDPegawai, TextEdit txtNama, ComboBoxEdit cmbStatusPendidikan, DateEdit dtTglLahir)
        {
            string tStrVal = string.Empty;
            string tJudul = "-Validation";

            if (txtIDPegawai.EditValue == null)
            {
                tStrVal = "Mohon mengisi ID Pegawai";
                clsMessage.WarningMessage(tJudul, tStrVal);
                return false;
            }
            else if(txtNama.EditValue == null)
            {
                tStrVal = "Mohon mengisi Nama Pegawai";
                clsMessage.WarningMessage(tJudul, tStrVal);
                return false;
            }
            else if (cmbStatusPendidikan.EditValue == null)
            {
                tStrVal = "Mohon memilih status pendidikan terakhir";
                clsMessage.WarningMessage(tJudul, tStrVal);
                return false;
            }
            else if (dtTglLahir.EditValue == null)
            {
                tStrVal = "Mohon mengisi tanggal lahir";
                clsMessage.WarningMessage(tJudul, tStrVal);
                return false;
            }

            return true;
        }
    }
}
