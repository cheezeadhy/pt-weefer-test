﻿using DevExpress.XtraEditors;
using nmPegawai;
using nmCtrl;
using nmCommon;
using nmEF;
using System;
using DevExpress.XtraSplashScreen;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraEditors.Controls;
using System.Windows.Forms;

namespace nmPegawai
{
    public partial class frmDPEGAWAI : XtraForm
    {
        string tmpDGIDPegawai;

        public frmDPEGAWAI()
        {
            SplashScreenManager.ShowForm(typeof(frmLoading));

            InitializeComponent();

            //EVENT HANDLER
            this.Load += new EventHandler(MyLoad);
            btnInput.Click += new EventHandler(MyInput);
            btnEdit.Click += new EventHandler(MyEdit);
            btnDelete.Click += new EventHandler(MyNonActive);
            btnKeluar.Click += new EventHandler(MyClose);
            btnReload.Click += new EventHandler(MyReload);


            dgView.CustomDrawCell += new RowCellCustomDrawEventHandler(CustomDrawCell);
        }

        private void MyLoad(object sender, EventArgs e)
        {
            try
            {
                //RETRIEVE PEGAWAI DATA
                clsPegawai.RETRIEVE_DATA(dg,dgView);
            }
            catch (Exception ex)
            {
                clsMessage.ErrorMessage(ex);
            }
            finally
            {
                if (SplashScreenManager.Default != null && SplashScreenManager.Default.IsSplashFormVisible)
                {
                    SplashScreenManager.CloseForm();
                }
            }
        }

        private void MyInput(object sender, EventArgs e)
        {
            try
            {
                //CALLING TRANSACTION FORM
                using (var frmDlg = new frmTPEGAWAI("NEW"))
                {
                    DialogResult dr = frmDlg.ShowDialog();
                    if (dr == DialogResult.OK)
                    {
                        //RETRIEVE PEGAWAI DATA
                        clsPegawai.RETRIEVE_DATA(dg, dgView);
                    }
                }
            }
            catch (Exception ex)
            {
                clsMessage.ErrorMessage(ex);
            }
        }

        private void MyEdit(object sender, EventArgs e)
        {
            try
            {
                if (dgView.RowCount > 0)
                {
                    GetRowCellValue();

                    //CALLING TRANSACTION FORM
                    using (var frmDlg = new frmTPEGAWAI("EDIT",tmpDGIDPegawai))
                    {
                        DialogResult dr = frmDlg.ShowDialog();
                        if (dr == DialogResult.OK)
                        {
                            //RETRIEVE PEGAWAI DATA
                            clsPegawai.RETRIEVE_DATA(dg, dgView);
                        }
                    }
                }
                else
                    clsMessage.WarningMessage(" | Can't Edit", "Data pegawai kosong, tidak ada yang bisa di-edit");
            }
            catch (Exception ex)
            {
                clsMessage.ErrorMessage(ex);
            }
        }

        private void GetRowCellValue()
        {
            try
            {
                if (dgView.RowCount > 0)
                {
                    //AMBIL NILAI KODE
                    tmpDGIDPegawai = Convert.ToString(dgView.GetFocusedRowCellValue("IDPegawai"));
                }

            }
            catch (Exception ex)
            {
                clsMessage.ErrorMessage(ex);
            }
        }

        private void MyNonActive(object sender, EventArgs e)
        {
            try
            {
                if (dgView.RowCount > 0)
                {
                    GetRowCellValue();

                    //CONFIRMATION
                    if (clsMessage.WarningConfirmation("Anda yakin akan menghapus data ini?") == false)
                        return;

                    using (var context = new EFEntities())
                    {
                        clsPegawai.NON_ACTIVE(tmpDGIDPegawai, context);
                        context.SaveChanges();
                        clsMessage.CustomSuccessMessage("Data berhasil dihapus");
                        //RETRIEVE PEGAWAI DATA
                        clsPegawai.RETRIEVE_DATA(dg, dgView);
                    }
                }
                else
                    clsMessage.WarningMessage(" | Can't Delete", "Data pegawai kosong, tidak ada yang bisa dihapus");               
            }
            catch (Exception ex)
            {
                clsMessage.ErrorMessage(ex);
            }
        }

        private void CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            clsGrid.Custom_Draw_Cell_Number("colNo", e);
        }

        private void MyClose(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MyReload(object sender, EventArgs e)
        {
            //RETRIEVE PEGAWAI DATA
            clsPegawai.RETRIEVE_DATA(dg, dgView);
        }
    }
}
