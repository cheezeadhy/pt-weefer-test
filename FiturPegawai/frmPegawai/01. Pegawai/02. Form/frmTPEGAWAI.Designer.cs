﻿namespace nmPegawai
{
    partial class frmTPEGAWAI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTPEGAWAI));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnReset = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.chkStatusAktif = new DevExpress.XtraEditors.CheckEdit();
            this.cmbStatusPendidikan = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dtTglLahir = new DevExpress.XtraEditors.DateEdit();
            this.txtNama = new DevExpress.XtraEditors.TextEdit();
            this.txtIDPegawai = new DevExpress.XtraEditors.TextEdit();
            this.txtAlamat = new DevExpress.XtraEditors.TextEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkStatusAktif.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatusPendidikan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTglLahir.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTglLahir.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNama.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIDPegawai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAlamat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(571, 295);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Data Pegawai";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnClose);
            this.layoutControl1.Controls.Add(this.btnReset);
            this.layoutControl1.Controls.Add(this.btnSave);
            this.layoutControl1.Controls.Add(this.chkStatusAktif);
            this.layoutControl1.Controls.Add(this.cmbStatusPendidikan);
            this.layoutControl1.Controls.Add(this.dtTglLahir);
            this.layoutControl1.Controls.Add(this.txtNama);
            this.layoutControl1.Controls.Add(this.txtIDPegawai);
            this.layoutControl1.Controls.Add(this.txtAlamat);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 21);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(567, 272);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnClose
            // 
            this.btnClose.Appearance.Options.UseFont = true;
            this.btnClose.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.btnClose.Location = new System.Drawing.Point(382, 217);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(170, 36);
            this.btnClose.StyleController = this.layoutControl1;
            this.btnClose.TabIndex = 111;
            this.btnClose.Text = "Close";
            // 
            // btnReset
            // 
            this.btnReset.Appearance.Options.UseFont = true;
            this.btnReset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.btnReset.Location = new System.Drawing.Point(202, 217);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(170, 36);
            this.btnReset.StyleController = this.layoutControl1;
            this.btnReset.TabIndex = 111;
            this.btnReset.Text = "Reset";
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnInput.ImageOptions.Image")));
            this.btnSave.Location = new System.Drawing.Point(15, 217);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(177, 36);
            this.btnSave.StyleController = this.layoutControl1;
            this.btnSave.TabIndex = 110;
            this.btnSave.Text = "Save";
            // 
            // chkStatusAktif
            // 
            this.chkStatusAktif.EditValue = true;
            this.chkStatusAktif.Location = new System.Drawing.Point(137, 185);
            this.chkStatusAktif.Name = "chkStatusAktif";
            this.chkStatusAktif.Properties.Caption = "Aktif / Non Aktif";
            this.chkStatusAktif.Size = new System.Drawing.Size(415, 22);
            this.chkStatusAktif.StyleController = this.layoutControl1;
            this.chkStatusAktif.TabIndex = 116;
            // 
            // cmbStatusPendidikan
            // 
            this.cmbStatusPendidikan.Location = new System.Drawing.Point(137, 151);
            this.cmbStatusPendidikan.Name = "cmbStatusPendidikan";
            this.cmbStatusPendidikan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatusPendidikan.Properties.Items.AddRange(new object[] {
            "SD",
            "SMP",
            "SMA",
            "S1"});
            this.cmbStatusPendidikan.Properties.NullText = "-- Pilih Pendidikan Terakhir --";
            this.cmbStatusPendidikan.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatusPendidikan.Size = new System.Drawing.Size(415, 24);
            this.cmbStatusPendidikan.StyleController = this.layoutControl1;
            this.cmbStatusPendidikan.TabIndex = 115;
            // 
            // dtTglLahir
            // 
            this.dtTglLahir.EditValue = null;
            this.dtTglLahir.Location = new System.Drawing.Point(137, 117);
            this.dtTglLahir.Name = "dtTglLahir";
            this.dtTglLahir.Properties.Appearance.Options.UseFont = true;
            this.dtTglLahir.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtTglLahir.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtTglLahir.Properties.CalendarTimeProperties.Mask.EditMask = "d";
            this.dtTglLahir.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtTglLahir.Size = new System.Drawing.Size(415, 24);
            this.dtTglLahir.StyleController = this.layoutControl1;
            this.dtTglLahir.TabIndex = 108;
            // 
            // txtNama
            // 
            this.txtNama.Location = new System.Drawing.Point(137, 49);
            this.txtNama.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNama.Name = "txtNama";
            this.txtNama.Properties.Appearance.Options.UseFont = true;
            this.txtNama.Size = new System.Drawing.Size(415, 24);
            this.txtNama.StyleController = this.layoutControl1;
            this.txtNama.TabIndex = 9;
            // 
            // txtIDPegawai
            // 
            this.txtIDPegawai.EditValue = "";
            this.txtIDPegawai.Location = new System.Drawing.Point(137, 15);
            this.txtIDPegawai.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtIDPegawai.Name = "txtIDPegawai";
            this.txtIDPegawai.Properties.Appearance.Options.UseFont = true;
            this.txtIDPegawai.Properties.ReadOnly = true;
            this.txtIDPegawai.Size = new System.Drawing.Size(415, 24);
            this.txtIDPegawai.StyleController = this.layoutControl1;
            this.txtIDPegawai.TabIndex = 8;
            // 
            // txtAlamat
            // 
            this.txtAlamat.Location = new System.Drawing.Point(137, 83);
            this.txtAlamat.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtAlamat.Name = "txtAlamat";
            this.txtAlamat.Properties.Appearance.Options.UseFont = true;
            this.txtAlamat.Size = new System.Drawing.Size(415, 24);
            this.txtAlamat.StyleController = this.layoutControl1;
            this.txtAlamat.TabIndex = 9;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(567, 272);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtIDPegawai;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem1.Size = new System.Drawing.Size(547, 34);
            this.layoutControlItem1.Text = "ID Pegawai";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(119, 18);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtNama;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem2.Size = new System.Drawing.Size(547, 34);
            this.layoutControlItem2.Text = "Nama";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(119, 18);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dtTglLahir;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 102);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem3.Size = new System.Drawing.Size(547, 34);
            this.layoutControlItem3.Text = "TglLahir";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(119, 18);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.cmbStatusPendidikan;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 136);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem4.Size = new System.Drawing.Size(547, 34);
            this.layoutControlItem4.Text = "Pendidikan Terakhir";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(119, 18);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.chkStatusAktif;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 170);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem5.Size = new System.Drawing.Size(547, 32);
            this.layoutControlItem5.Text = "Status Pegawai";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(119, 18);
            this.layoutControlItem5.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtAlamat;
            this.layoutControlItem7.CustomizationFormText = "Nama";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 68);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem7.Size = new System.Drawing.Size(547, 34);
            this.layoutControlItem7.Text = "Alamat";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(119, 18);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnSave;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 202);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem6.Size = new System.Drawing.Size(187, 50);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnReset;
            this.layoutControlItem8.Location = new System.Drawing.Point(187, 202);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem8.Size = new System.Drawing.Size(180, 50);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btnClose;
            this.layoutControlItem9.Location = new System.Drawing.Point(367, 202);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem9.Size = new System.Drawing.Size(180, 50);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // frmTPEGAWAI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 295);
            this.Controls.Add(this.groupControl1);
            this.Name = "frmTPEGAWAI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "T Pegawai";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkStatusAktif.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatusPendidikan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTglLahir.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTglLahir.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNama.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIDPegawai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAlamat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.CheckEdit chkStatusAktif;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatusPendidikan;
        internal DevExpress.XtraEditors.DateEdit dtTglLahir;
        internal DevExpress.XtraEditors.TextEdit txtNama;
        internal DevExpress.XtraEditors.TextEdit txtIDPegawai;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit txtAlamat;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        internal DevExpress.XtraEditors.SimpleButton btnClose;
        internal DevExpress.XtraEditors.SimpleButton btnReset;
        internal DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
    }
}