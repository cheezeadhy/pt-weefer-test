﻿using DevExpress.XtraEditors;
using nmPegawai;
using nmCtrl;
using nmEF;
using nmCommon;
using System;
using DevExpress.XtraSplashScreen;
using DevExpress.XtraEditors.Controls;
using System.Windows.Forms;

namespace nmPegawai
{
    public partial class frmTPEGAWAI : XtraForm
    {
        string tmpStatus, tmpIDPegawai;
        Boolean tmpClosingStatus = false;

        public frmTPEGAWAI(string pStatus, string pIDPegawai = "")
        {
            SplashScreenManager.ShowForm(typeof(frmLoading));

            InitializeComponent();

            tmpStatus = pStatus;
            tmpIDPegawai = pIDPegawai;

            //EVENT HANDLER
            this.Load += new EventHandler(MyLoad);
            btnSave.Click += new EventHandler(MySave);
            btnReset.Click += new EventHandler(MyReset);
            btnClose.Click += new EventHandler(MyClose);
        }

        private void MyLoad(object sender, EventArgs e)
        {
            try
            {
               if(tmpStatus == "NEW")
                {
                    //SET DEFAULT ID PEGAWAI
                    txtIDPegawai.EditValue = clsPegawai.GEN_CODE();
                    dtTglLahir.EditValue = DateTime.Now;
                }
                else if (tmpStatus == "EDIT")
                {
                    //RETRIEVE DATA TO CONTROL
                    clsPegawai.RETRIEVE_TO_CONTROL(tmpIDPegawai,txtIDPegawai, txtNama, txtAlamat, dtTglLahir, cmbStatusPendidikan, chkStatusAktif);
                }
            }
            catch (Exception ex)
            {
                clsMessage.ErrorMessage(ex);
            }
            finally
            {
                if (SplashScreenManager.Default != null && SplashScreenManager.Default.IsSplashFormVisible)
                {
                    SplashScreenManager.CloseForm();
                }
            }
        }

        private void MySave(object sender, EventArgs e)
        {
            try
            {
                //VALIDATION
                if (clsPegawai.VALIDATION(txtIDPegawai, txtNama, cmbStatusPendidikan, dtTglLahir) == false)
                    return;

                if (clsMessage.Confirmation("Anda yakin akan menyimpan data ini?") == false)
                    return;

                //DECLARE VARIABLE
                var tIDPegawai = clsControl.SET_EMPTY_IF_NOTHING(txtIDPegawai);
                var tNama = clsControl.SET_EMPTY_IF_NOTHING(txtNama);
                var tAlamat = clsControl.SET_EMPTY_IF_NOTHING(txtAlamat);
                var tTglLahir = Convert.ToDateTime(dtTglLahir.EditValue);
                var tPendidikanTerakhir = Convert.ToString(cmbStatusPendidikan.EditValue);
                var tStatusAktif = Convert.ToBoolean(chkStatusAktif.EditValue);

                using (var context = new EFEntities())
                {
                    if(tmpStatus == "NEW")
                    {
                        //ADD ENTITY
                        var cPEGAWAi = new clsPegawai();
                        cPEGAWAi.tIDPegawai = tIDPegawai;
                        cPEGAWAi.tNama = tNama;
                        cPEGAWAi.tAlamat = tAlamat;
                        cPEGAWAi.tTglLahir = tTglLahir;
                        cPEGAWAi.tPendidikanTerakhir = tPendidikanTerakhir;
                        cPEGAWAi.tStatusAktif = tStatusAktif;
                        cPEGAWAi.CREATE(context);
                    }
                    else if (tmpStatus == "EDIT")
                    {
                        //EDIT ENTITY
                        var cPEGAWAi = new clsPegawai();
                        cPEGAWAi.tNama = tNama;
                        cPEGAWAi.tAlamat = tAlamat;
                        cPEGAWAi.tTglLahir = tTglLahir;
                        cPEGAWAi.tPendidikanTerakhir = tPendidikanTerakhir;
                        cPEGAWAi.tStatusAktif = tStatusAktif;
                        cPEGAWAi.UPDATE(tIDPegawai, context);
                    }

                    //SAVE DATA
                    context.SaveChanges();
                    //SHOW SUCCESS MESSAGE
                    clsMessage.SuccessMessage();
                    //CLEAR ALL CONTROL
                    MyClear_Control();
                    tmpClosingStatus = true;
                    btnClose.PerformClick();
                }
            }
            catch (Exception ex)
            {
                clsMessage.ErrorMessage(ex);
            }
        }

        private void MyClose(object sender, EventArgs e)
        {
            try
            {
                if (this.Modal)
                {
                    if (tmpClosingStatus == false)
                        this.DialogResult = DialogResult.Abort;
                    else
                        this.DialogResult = DialogResult.OK;
                }
                else
                    this.Close();
            }
            catch (Exception ex)
            {
                clsMessage.ErrorMessage(ex);
            }
        }

        private void MyReset(object sender, EventArgs e)
        {
            try
            {
                MyClear_Control();
                if (tmpStatus == "EDIT")
                    txtIDPegawai.EditValue = tmpIDPegawai;
            }
            catch (Exception ex)
            {
                clsMessage.ErrorMessage(ex);
            }
        }

        private void MyClear_Control()
        {
            txtIDPegawai.EditValue = clsPegawai.GEN_CODE();
            txtNama.EditValue = null;
            txtAlamat.EditValue = null;
            dtTglLahir.EditValue = DateTime.Now;
            cmbStatusPendidikan.EditValue = null;
            chkStatusAktif.Checked = true;
            txtNama.Focus();
        }

    }
}